<div class="bg-gray w-full">
  <div class="container grid grid-cols-5 items-top gap-20 sm:px-8 sm:py-20">
    <div class="text-white ">
      <div class="flex flex-col gap-y-10">
        <img src="{{asset('svg/favico.svg')}}" alt="favicon du logo" class="w-10"/>
      </div>
      <p>Prenez vôtre place dans le marché du numérique !</p>
      <div class="flex items-center gap-5 text-xl text-gray-800 ">
        <a href="http://" target="_blank" rel="noopener noreferrer">
          <i class="fa-brands fa-facebook"></i>
        </a>
        <a href="http://" target="_blank" rel="noopener noreferrer">
          <i class="fa-brands fa-instagram"></i>
        </a>
        <a href="http://" target="_blank" rel="noopener noreferrer">
          <i class="fa-brands fa-linkedin"></i>
        </a>
        <a href="http://" target="_blank" rel="noopener noreferrer">
          <i class="fa-brands fa-pinterest "></i>
        </a>
      </div>
    </div>
    <div class="text-white">
      <h6 class="text-xl mb-5 font-semibold">Apps</h6>
      <ul id="footerList">
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
      </ul>
    </div>
    <div class="text-white">
      <h6 class="text-xl mb-5 font-semibold">Liens utiles</h6>
      <ul id="footerList">
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
      </ul>
    </div>
    <div class="text-white">
      <h6 class="text-xl mb-5 font-semibold">Legal</h6>
      <ul id="footerList">
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
      </ul>
    </div>
    <div class="text-white">
      <h6 class="text-xl mb-5 font-semibold">Legal</h6>
      <ul id="footerList">
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
        <li>
          <a href="#">Hello</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="border-t border-gray">
    <span>Copyright {{date("Y")}} | Tous droits réservés à Digitweb </span>
  </div>
</div>