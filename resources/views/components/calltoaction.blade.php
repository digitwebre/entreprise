<div class="min-w-7xl mx-auto  flex relative z-10 items-center overflow-hidden h-full">
  <div class="container mx-auto px-6 flex items-center relative py-16 gap-32">
      <div class="sm:w-2/3 lg:w-2/5 flex flex-col justify-center relative z-20">
          <span class="w-20 h-2 bg-primary dark:bg-white mb-5">
          </span>
          <h1 class="text-6xl sm:text-7xl font-bold sm:font-bold flex flex-col leading-none dark:text-gray text-gray">
              Boostez votre marque grâce à internet
              {{-- <span class="text-5xl sm:text-7xl"></span> --}}
          </h1>
          <p class="text-sm sm:text-base text-gray-700 dark:text-white">
              Grâce à Digitweb , prenez vôtre place dans le marché du numérique !
          </p>
          <div class="flex mt-8">
              <a href="#" class="uppercase py-2 px-4 rounded-lg border-2 border-gray text-gray text-md mr-4 hover:bg-gray hover:text-white">
                  Qui sommes-nous ?
              </a>
              <a href="#" class="uppercase py-2 px-4 rounded-lg bg-gray border-2 border-pink-500 text-white dark:text-white hover:bg-white hover:border-primary hover:text-primary text-md">
                  Nous contactez
              </a>
          </div>
      </div>
      <div class="hidden sm:block sm:w-1/3 lg:w-3/5 relative">
          <img src="{{asset('images/rokcet.png')}}" class="max-w-xs md:max-w-xl m-auto"/>
          {{-- <img src="https://www.tailwind-kit.com/images/object/10.png" class="max-w-xs md:max-w-sm m-auto"/> --}}
      </div>
  </div>
</div>