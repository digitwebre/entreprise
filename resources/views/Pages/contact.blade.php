@extends('app')

@section('content')
<div class="max-w-7xl mx-auto h-full grid grid-cols-1 sm:grid-cols-2 items-center isolate">
  <div class="order-2 sm:order-1 text-center sm:text-start">
    <h1 class="text-3xl sm:text-6xl font-bold">Vous souhaitez obtenir une information ou un devis ?</h1>
    <p class="text-gray-600 my-5">Contactez nous via le formulaire ci-dessous pour obtenir vos informations et recevez vôtre réponse sous 24 heures !</p>
    <a
      href="#contact_form"
      class="hover:shadow-form w-full rounded-md bg-gray py-3 px-8 text-center text-base font-semibold text-white outline-none"
    >
      Aller jusqu'au formulaire
    </a>
  </div>
  <div>
    <img src="{{asset('images/contact.png')}}" alt="" class="w-[100vh] sm:w-full"/>
  </div>
</div>
<div class="flex items-center justify-center p-12" id="contact_form">
  <!-- Author: FormBold Team -->
  <div class="grid grid-cols-1 lg:grid-cols-1 sm:w-1/2" >
    <div class="mx-auto w-full max-w-7xlmax-w-[550px] bg-white">
        <form >
            <div class="mb-5">
                <label for="name" class="mb-3 block text-base font-medium text-primary">
                    Nom & Prénom
                </label>
                <input type="text" name="name" id="name" placeholder="Nom & Prénom"
                    class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
            </div>
            <div class="mb-5">
              <label for="name" class="mb-3 block text-base font-medium text-primary">
                  Raison sociale de l'entreprise <span class="text-gray-500 font-light">Facultatif</span>
              </label>
              <input type="text" name="name" id="name" placeholder="Raison sociale de l'entreprise"
                  class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
          </div>
            <div class="mb-5">
                <label for="phone" class="mb-3 block text-base font-medium text-primary">
                    N° de téléphone
                </label>
                <input type="text" name="phone" id="phone" placeholder="Enter your phone number"
                    class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
            </div>
            <div class="mb-5">
              <label for="email" class="mb-3 block text-base font-medium text-primary">
                  Adresse email
              </label>
              <input type="email" name="email" id="email" placeholder="Enter your email"
                  class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
            </div>
            <div class="mb-5">
              <label for="ile_de_la_reunion" class="mb-3 block text-base font-medium text-primary">
                  Êtes vous de l'île de La Réunion ?
              </label>
              <select name="ile_de_la_reunion" class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md">
                <option>Oui, je suis de la Réunion</option>
                <option>Non, je viens d'ailleurs</option>
              </select>
            </div>
            <div class="-mx-3 flex-wrap">
                <div class="w-full px-3">
                  <div class="mb-5">
                    <label for="demande_for" class="mb-3 block text-base font-medium text-primary">
                        Ma demande concerne...
                    </label>
                    <select name="demande_for" class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md">
                      <option>Une information</option>
                      <option>Une demande de devis</option>
                      <option>Une demande de stage</option>
                      <option>Autre</option>
                    </select>
                  </div>
                </div>
                <div class="w-full px-3 sm:w-full">
                    <div class="mb-5">
                        <label for="message" class="mb-3 block text-base font-medium text-primary">
                            Votre message / demande
                        </label>
                        <textarea name="message" id="message" class="w-full sm:w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"></textarea>
                    </div>
                </div>
            </div>

            {{-- <div class="mb-5 pt-3">
                <label class="mb-5 block text-base font-semibold text-primary sm:text-xl">
                    Address Details
                </label>
                <div class="-mx-3 flex flex-wrap">
                    <div class="w-full px-3 sm:w-full">
                        <div class="mb-5">
                            <input type="text" name="area" id="area" placeholder="Enter area"
                                class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                        </div>
                    </div>
                    <div class="w-full px-3 sm:w-full">
                        <div class="mb-5">
                            <input type="text" name="city" id="city" placeholder="Enter city"
                                class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                        </div>
                    </div>
                    <div class="w-full px-3 sm:w-full">
                        <div class="mb-5">
                            <input type="text" name="state" id="state" placeholder="Enter state"
                                class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                        </div>
                    </div>
                    <div class="w-full px-3 sm:w-full">
                        <div class="mb-5">
                            <input type="text" name="post-code" id="post-code" placeholder="Post Code"
                                class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                        </div>
                    </div>
                </div>
            </div> --}}

            <div>
                <button
                    class="hover:shadow-form w-full sm:w-full rounded-md bg-[#6A64F1] py-3 px-8 text-center text-base font-semibold text-white outline-none">
                    Envoyer ma fiche contacte
                </button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection