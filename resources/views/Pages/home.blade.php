@extends('app')
@section('content')
  @include('components.calltoaction')

  <!-- Qui sommes nous ? -->
  <div class="max-w-7xl mx-auto relative z-10 overflow-hidden  h-screen items-center flex justify-center">
    <div class="text-start bg-[url('../../../../public/svg/cloud.svg')] bg-contain bg-no-repeat py-52">
      <h1 class="text-7xl mb-10">Qui sommes-nous ?</h1>
      <p class="text-justify">
        Digitweb est une entreprise créée en 2024, par Djebarlen TAMBON dans le domaine de la création de site internet et créateur visuel.
        Fondée sur une vision avant-gardiste en 2024, Digitweb s'est rapidement imposée comme une référence incontournable dans la conception de sites internet et la création d'images digitales. Notre entreprise incarne l'essence même de la modernité et de la créativité, repoussant sans cesse les limites de l'imagination pour offrir à nos clients des solutions numériques d'une qualité exceptionnelle. En associant une expertise technique de pointe à une sensibilité artistique affûtée, nous façonnons des sites web qui captivent et des images qui inspirent. Que vous soyez une start-up ambitieuse cherchant à marquer votre présence en ligne ou une entreprise établie désireuse de se réinventer, Digitweb vous accompagne dans chaque étape de votre parcours digital. Avec notre équipe passionnée et notre engagement indéfectible envers l'excellence, nous sommes prêts à transformer votre vision en une réalité digitale saisissante. Rejoignez-nous chez Digitweb, où l'avenir de votre présence en ligne commence aujourd'hui.
      </p>
      <br/>
      <p class="text-justify">
        Au cœur de notre ADN réside une passion inébranlable pour l'innovation et la créativité, propulsant nos services de conception de sites internet et d'imagerie digitale vers de nouveaux sommets. Nous sommes bien plus qu'une simple entreprise : nous sommes les architectes de votre présence en ligne, les sculpteurs de votre identité digitale. Forts d'une équipe de talents multidisciplinaires, nous fusionnons expertise technique et flair artistique pour façonner des expériences digitales incomparables. Chez Digitweb, chaque projet est une toile vierge sur laquelle nous inscrivons les rêves et les aspirations de nos clients, transformant leurs idées en réalités virtuelles époustouflantes. Que vous soyez une start-up ambitieuse à la recherche d'un lancement percutant ou une entreprise établie désireuse de se réinventer, nous sommes là pour donner vie à vos visions les plus audacieuses. Rejoignez-nous chez Digitweb et laissez-nous vous guider vers un avenir digital radieux où l'exceptionnel devient la norme.
      </p>
      <a href="#" class="">En apprendre plus</a>
    </div>
  </div>
@endsection