@extends('app')
@section('content')
  <section class="sm:mx-auto relative top-[20vh] min-w-5xl lg:max-w-7xl h-screen bg-transparent">
    <div>
      <span class="flex justify-center items-center text-8xl">
        <p class="text-[240px] font-extrabold text-primary opacity-25">4</p>
        <img src="{{asset('svg/boussole.svg')}}" class="w-52" />
        <p class="text-[240px] font-extrabold text-primary opacity-25">4</p>
      </span>
      <div class="flex flex-col gap-y-10">
        <p class="text-5xl font-medium text-center ">Oh non ! Il semble que cette page n'existe pas !</p>
        <a href="{{route('home')}}" class="hover:shadow-form inline-block rounded-md bg-[#6A64F1] py-3 px-8 text-center text-base font-semibold text-white outline-none">Retourner à l'accueil</a>
      </div>
    </div>
  </section>
@endsection