import './bootstrap';
var button = document.getElementById('mobile-nav');
var menu = document.getElementById('mobile-menu');
var hamburgerBtn = document.getElementById('open');
var closeBtn = document.getElementById('close');
var bool = false;

button.addEventListener('click', () => {
  bool = !bool;
  {bool ? (menu.classList.replace('hidden', 'flex')) : (menu.classList.replace('flex', "hidden"))}
  {bool ? (hamburgerBtn.classList.replace('block', 'hidden')) && (closeBtn.classList.replace('hidden', 'block')) : (closeBtn.classList.replace('block', 'hidden')) && (hamburgerBtn.classList.replace('hidden', 'block'))}

})

// Smooth Scroll on click on an Anchor
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
  anchor.addEventListener('click', function (e) {
      e.preventDefault();

      document.querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth'
      });
  });
});