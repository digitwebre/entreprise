<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('Pages.home');
})->name('home');

Route::get('contact', function(){
    return view('Pages.contact');
})->name('contact.index');

Route::get('error', function(){
    return view('Pages.Error_pages.404');
});